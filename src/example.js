module.exports = [
  {
    Title: "My awesome hire company",
    LatLon: ["57.113171", "-3.1034056"],
    Type: 2,
    IconCategory: "hire-rental",
    Excerpt:
      "Example hire-rental",
    Address: "My Awesome Hire, Victoria Road, Forres, IV11 1BN",
    Phone: "555 000 111 222",
    Website: "https://my-awesome-hire.co.uk"
  },
  {
    Title: "My awesome accomodation example",
    LatLon: ["57.1833702747768", "-4.165308157017921"],
    Type: 2,
    IconCategory: "accommodation",
    Excerpt:
      "Accomodation example",
    Address: "Accomodation, Aspen House, Ardgay, Sutherland IV11 1BG",
    Phone: "555 987 654 321",
    Website: "http://accomodation.co.uk/",
  },
];