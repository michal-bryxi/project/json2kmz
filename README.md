# JSON 2 kmz

Found great map online? Want to use the points in your favourite mapping app ([Locus](https://www.locusmap.eu/))? If you are able to extract the JSON object, this repo can then help you converting the data points to *.kmz.

This is not a drop-in functionality (at the moment). You _will_ have to know a little bit of coding to make it work.

![Screenshot of Locus with example data](./screen.jpeg)

## Setup

### Prepare repo

```sh
git clone git@gitlab.com:michal.bryxi/json2kmz.git
cd json2kmz
yarn
```

### Icons

They live in `src/images`

### JSON source data

They live in `src/doc.js` as a _node module_. 

See `src/example.js` to see a **working** example. You can just copy this file to `src/doc.js` and try to run the script.

## How to use

```sh
npm start
```

Now take a look at `output.kmz`.