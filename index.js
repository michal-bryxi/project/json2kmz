const convert = require("xml-js");
const source = require("./src/doc.js");
const { zip } = require("zip-a-folder");
const fs = require("fs-extra");

async function main() {
  // Extracting icons
  let iconsList = source.map(item => {
    let type = item.Type === 2 ? "-paid" : "";
    return `${item.IconCategory}${type}`;
  });
  let iconsUniq = Array.from(new Set(iconsList));
  let icons = iconsUniq.map(item => {
    return {
      _attributes: {
        id: item
      },
      IconStyle: {
        Icon: {
          href: {
            _text: `images/pin-${item}.png`
          }
        },
        hotSpot: {
          _attributes: {
            x: "0.5",
            y: "0.0",
            xunits: "fraction",
            yunits: "fraction"
          }
        }
      }
    };
  });

  // Extracting data
  let data = source.map(item => {
    let address = item.Address.split(",");
    let type = item.Type === 2 ? "-paid" : "";
    if (item.LatLon) {
      return {
        name: item.Title,
        description: `${item.Excerpt}, ${item.Website}, ${item.Phone}`,
        styleUrl: {
          _text: `#${item.IconCategory}${type}`
        },
        ExtendedData: {
          Data: [
            {
              _attributes: {
                name: "locus_address_street"
              },
              displayName: {
                _text: "Street"
              },
              value: {
                _text: address[1]
              }
            },
            {
              _attributes: {
                name: "locus_address_city"
              },
              displayName: {
                _text: "City"
              },
              value: {
                _text: address[2]
              }
            },
            {
              _attributes: {
                name: "locus_address_psc"
              },
              displayName: {
                _text: "Post code"
              },
              value: {
                _text: address[3]
              }
            }
          ]
        },
        Point: {
          coordinates: {
            _text: `${item.LatLon[1]},${item.LatLon[0]}`
          }
        }
      };
    }
  });

  // Completing KML
  let json = {
    _declaration: {
      _attributes: {
        version: "1.0",
        encoding: "utf-8"
      }
    },
    kml: {
      _attributes: {
        xmlns: "http://www.opengis.net/kml/2.2",
        "xmlns:gx": "http://www.google.com/kml/ext/2.2",
        "xmlns:atom": "http://www.w3.org/2005/Atom"
      },
      Document: {
        name: "JSON -> KMZ conversion",
        Style: icons,
        Placemark: data
      }
    }
  };

  // JSON => KML
  let options = { compact: true, ignoreComment: true, spaces: 4 };
  result = convert.json2xml(json, options);

  // Write KML
  fs.writeFile("dst/doc.kml", result, error => {
    console.log(error);
  });

  // Sync images
  await fs.remove("dst/images");
  await fs.copy("src/images", "dst/images");

  // Create KMZ
  await zip("dst", "output.kmz");

  // Yay!
  console.log("👍🏻 Export is done: ./output.kmz");
}

main();
